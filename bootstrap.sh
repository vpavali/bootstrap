#!/bin/bash

set -e

function at_error
{
	echo "\033[032mfailed\033[0m"
}

function info
{
    echo -e "\033[01m$@\033[0m"
}

trap at_error ERR

# check

echo -n "checking docker..."

command -v docker >/dev/null 2>&1 || { echo >&2 "docker is required but it's not installed.  Aborting."; exit 1; }
docker run --rm hello-world >/dev/null 2>&1 || { echo >&2 "docker is required but it seems to be broken (docker run hello-world failed).  Aborting."; exit 1; }

echo "ok"

# pull

info "==> pulling docker images"
./pull.sh
info "<== pulling docker images done"

# init

./init/ceph
./init/mongodata
./init/segments

# launch

info "==> running docker images"
./run.sh
info "<== running docker images done"
